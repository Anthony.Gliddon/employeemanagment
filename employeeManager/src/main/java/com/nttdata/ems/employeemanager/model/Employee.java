package com.nttdata.ems.employeemanager.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Anthony Gliddon
 * @author Bhanu
 * @author Roopali 
 *
 */

public class Employee implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int empId;
	private String firstName;
	private String lastName;
	private int departmentId;
	private double salary;
	private Address address;
	private Benefits benefits;
	private String emailId; 
	private float leaveRequested;
	private float leaveRemaining;
	private Date dob;
	
	public Employee() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	public Employee(int empId, String firstName, String lastName, int departmentId, double salary, Address address,
			Benefits benefits, String emailId, float leaveRequested, float leaveRemaining, Date dob) {
		super();
		this.empId = empId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.departmentId = departmentId;
		this.salary = salary;
		this.address = address;
		this.benefits = benefits;
		this.emailId = emailId;
		this.leaveRequested = leaveRequested;
		this.leaveRemaining = leaveRemaining;
		this.dob = dob;
	}




	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Benefits getBenefits() {
		return benefits;
	}
	public void setBenefits(Benefits benefits) {
		this.benefits = benefits;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public float getLeaveRequested() {
		return leaveRequested;
	}
	public void setLeaveRequested(float leaveRequested) {
		this.leaveRequested = leaveRequested;
	}
	public float getLeaveRemaining() {
		return leaveRemaining;
	}
	public void setLeaveRemaining(float leaveRemaining) {
		this.leaveRemaining = leaveRemaining;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}




	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", firstName=" + firstName + ", lastName=" + lastName + ", departmentId="
				+ departmentId + ", salary=" + salary + ", address=" + address + ", benefits=" + benefits + ", emailId="
				+ emailId + ", leaveRequested=" + leaveRequested + ", leaveRemaining=" + leaveRemaining + ", dob=" + dob
				+ "]";
	}




	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((benefits == null) ? 0 : benefits.hashCode());
		result = prime * result + departmentId;
		result = prime * result + ((dob == null) ? 0 : dob.hashCode());
		result = prime * result + ((emailId == null) ? 0 : emailId.hashCode());
		result = prime * result + empId;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + Float.floatToIntBits(leaveRemaining);
		result = prime * result + Float.floatToIntBits(leaveRequested);
		long temp;
		temp = Double.doubleToLongBits(salary);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}




	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (benefits == null) {
			if (other.benefits != null)
				return false;
		} else if (!benefits.equals(other.benefits))
			return false;
		if (departmentId != other.departmentId)
			return false;
		if (dob == null) {
			if (other.dob != null)
				return false;
		} else if (!dob.equals(other.dob))
			return false;
		if (emailId == null) {
			if (other.emailId != null)
				return false;
		} else if (!emailId.equals(other.emailId))
			return false;
		if (empId != other.empId)
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (Float.floatToIntBits(leaveRemaining) != Float.floatToIntBits(other.leaveRemaining))
			return false;
		if (Float.floatToIntBits(leaveRequested) != Float.floatToIntBits(other.leaveRequested))
			return false;
		if (Double.doubleToLongBits(salary) != Double.doubleToLongBits(other.salary))
			return false;
		return true;
	}
	
	
}
