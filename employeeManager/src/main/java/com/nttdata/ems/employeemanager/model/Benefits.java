package com.nttdata.ems.employeemanager.model;

public class Benefits {
	
	public final String PENSION = "Pension";
	public final double PENSION_COST = 20.99;
	
	public final String INSURANCE = "Insurance";
	public final double INSURANCE_COST = 15.25;

}
