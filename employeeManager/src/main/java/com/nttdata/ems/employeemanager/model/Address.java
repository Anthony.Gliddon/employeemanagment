package com.nttdata.ems.employeemanager.model;

public class Address {
	
	private int houseNumber;
	private String streetName;
	private String secondLine;
	private String city;
	private String postcode;
	
	public Address() {
	}

	
	
	public Address(int houseNumber, String streetName, String city, String postcode) {
		super();
		this.houseNumber = houseNumber;
		this.streetName = streetName;
		this.city = city;
		this.postcode = postcode;
	}



	public Address(String streetName, String secondLine, String city, String postcode) {
		super();
		this.streetName = streetName;
		this.secondLine = secondLine;
		this.city = city;
		this.postcode = postcode;
	}



	public Address(int houseNumber, String streetName, String secondLine, String city, String postcode) {
		super();
		this.houseNumber = houseNumber;
		this.streetName = streetName;
		this.secondLine = secondLine;
		this.city = city;
		this.postcode = postcode;
	}

	public int getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(int houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getSecondLine() {
		return secondLine;
	}

	public void setSecondLine(String secondLine) {
		this.secondLine = secondLine;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	@Override
	public String toString() {
		return "Address [houseNumber=" + houseNumber + ", streetName=" + streetName + ", secondLine=" + secondLine
				+ ", city=" + city + ", postcode=" + postcode + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + houseNumber;
		result = prime * result + ((postcode == null) ? 0 : postcode.hashCode());
		result = prime * result + ((secondLine == null) ? 0 : secondLine.hashCode());
		result = prime * result + ((streetName == null) ? 0 : streetName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (houseNumber != other.houseNumber)
			return false;
		if (postcode == null) {
			if (other.postcode != null)
				return false;
		} else if (!postcode.equals(other.postcode))
			return false;
		if (secondLine == null) {
			if (other.secondLine != null)
				return false;
		} else if (!secondLine.equals(other.secondLine))
			return false;
		if (streetName == null) {
			if (other.streetName != null)
				return false;
		} else if (!streetName.equals(other.streetName))
			return false;
		return true;
	}
	


	
	
	
}
